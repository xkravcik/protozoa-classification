# Project tree


 * [features](./features)  
        This folder contains datasets of extracted features
   * [call_ALT](./features/call_ALT)  
            Features of captures that used call videos, only STUN or DTLSv1.2
   * [call_UDP](./features/call_UDP)  
            Features of captures that used call videos, only raw UDP
   * [dashcam_ALT](./features/dashcam_ALT)  
            Features of captures that used dashcam videos, only STUN or DTLSv1.2
   * [dashcam_UDP](./features/dashcam_UDP)  
            Features of captures that used dashcam videos, only raw UDP
 * [chromium_builds](./chromium_builds)  
          In this .zip archive are both Chromium builds that we used for traffic capture.
     * [builds.zip](./chromium_builds/builds.zip)
 * [scripts](./scripts)
    * [classification.py](./scripts/classification.py)  
         This script was used to perform classification.
    * [classification_video_content.py](./scripts/classification_video_content.py)  
         This script was used to perform classification based on video content.
         It is mostly a copy of the [classification.py](./scripts/classification.py) script with a few differences.
    * [extraction.py](./scripts/extraction.py)  
        This script was used to extract features from each .pcap capture file.  
        This file is an adapted version of a script taken from https://github.com/dmbb/Protozoa/blob/master/analytics/feature_extractor.py
    * [traffic_capture_scripts](./scripts/traffic_capture_scripts)  
        These files were used to capture .pcap files, each one on its respective VM
        * [client.py](./scripts/traffic_capture_scripts/client.py)
        * [controller.py](./scripts/traffic_capture_scripts/controller.py)
        * [server.py](./scripts/traffic_capture_scripts/server.py)
 * [traffic_capture_files](./traffic_capture_files)  
        Here are some of the traffic capture (.pcap) files. The whole dataset is more than 22GB large and therefore does not fit here.
   * [caps_call](./traffic_capture_files/caps_call)
   * [caps_dashcam](./traffic_capture_files/caps_dashcam)
 

To try Protozoa, you should use the instructions and Vagrant VMs provided in https://github.com/dmbb/Protozoa  
We offer the option to download both Chromium builds because of the large amount of time and resources it takes to compile them yourself.  
The classification and extraction scripts may require a specific folder structure that decides what data the script reads.
