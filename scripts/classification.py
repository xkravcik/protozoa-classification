import csv
import itertools
import os
import random
import sys

import matplotlib.pyplot as plt
import numpy as np

from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import accuracy_score, roc_curve, auc
from sklearn.model_selection import StratifiedKFold, GridSearchCV

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier

models = [
    XGBClassifier(),
    # RandomForestClassifier(),
    # DecisionTreeClassifier()
]

feature_names = []
file_names = []

def readCSV(file_path):
    file_names.clear()
    file = open(file_path, 'r')
    lst = list(csv.reader(file, delimiter=','))
    data = lst[1:]
    feature_names.append(lst[0])
    data_np = np.array(data)[:, 1:-1].astype(float)
    # label = data[0][-1]
    for row in data:
        file_names.append(row[0])
    file.close()
    for i, file in enumerate(file_names):
        num = file.split('_')[2] if 'dark' in file else file.split('_')[2][:-4]
        type = 'r' if 'regular' in file else 'p'
        col = 'd' if 'dark' in file else 'b'
        file_names[i] = num + '_' + type + col
    return data_np


class CustomSplit:
    def __init__(self, files, n_splits=5):
        self.files = files
        self.n_splits = n_splits

    def split(self, dataset, labels, groups=None):

        # mode 1: Baseline test
        # mode 2: Overlapped videos test
        # mode 3: Augmented videos test

        if min(dataset[0]) >= 0:  # PL
            mode = 1
        else:  # Stats
            mode = 0
        # mode = 4

        n_splits = self.n_splits

        if mode == 0:
            skf = StratifiedKFold(n_splits=n_splits, shuffle=True)
            for train_index, test_index in skf.split(dataset, labels):
                yield train_index, test_index

        assert len(labels) == len(self.files)
        assert sum(labels) == len(labels) // 2

        structure = []
        for i in range(len(labels)):
            structure.append((i, labels[i], self.files[i], int(self.files[i].split("_")[0])))

        structure_unchanged = structure[:][:]

        if mode == 1 or mode == 2:
            tmp = [x for x in structure if 'd' not in x[2]]
            structure.clear()
            structure.extend(tmp)

            structure.sort(key=(lambda x: x[1]))
            structure.sort(key=(lambda x: x[3]))

            length = len(structure) // 2
            rand_offset = [0 for x in range(length // 2)] + [1 for x in range(length // 2)]
            random.shuffle(rand_offset)

            fin_set = []
            for x in range(0, len(structure) - 1, 2):
                fin_set.append(structure[x + rand_offset[x // 2]])
            random.shuffle(fin_set)

            fin_set.sort(key=(lambda x: x[1]))
            for i in range(0, length // 2, 2):
                fin_set[i], fin_set[i + length // 2] = fin_set[i + length // 2], fin_set[i]

            if mode == 1:
                jump = len(fin_set) // n_splits
                for i in range(n_splits):
                    index_window = ((i * jump), ((i + 1) * jump - 1))
                    train_index = [fin_set[x][0] for x in range(len(fin_set)) if x < index_window[0] or x > index_window[1]]
                    test_index = [fin_set[x][0] for x in range(len(fin_set)) if index_window[0] <= x <= index_window[1]]
                    assert len(set(train_index).union(set(test_index))) == len(fin_set)
                    assert len(set(train_index).intersection(set(test_index))) == 0
                    assert sum([labels[x] for x in train_index]) == len(train_index) // 2
                    assert sum([labels[x] for x in test_index]) == len(test_index) // 2
                    yield train_index, test_index

            if mode == 2:
                jump = len(fin_set) // n_splits
                for i in range(n_splits):
                    index_window = ((i * jump), ((i + 1) * jump - 1))
                    index_window_ignored = (((i - 1) * jump), (i * jump - 1)) if i != 0 else (((n_splits - 1) * jump), (n_splits * jump - 1))
                    train_index = [fin_set[x][0] for x in range(len(fin_set)) if x < index_window_ignored[0] or x > index_window_ignored[1]]
                    test_index = [structure[structure.index(list(filter((lambda a: a[3] == fin_set[x][3] and a[0] not in train_index), structure))[0])][0] for x in range(len(fin_set)) if
                                  index_window[0] <= x <= index_window[1]]
                    assert len(set(train_index).union(set(test_index))) == len(fin_set)
                    assert len(set(train_index).intersection(set(test_index))) == 0
                    assert set([structure_unchanged[x][3] for x in train_index]).intersection(set([structure_unchanged[x][3] for x in test_index])) == \
                           set([structure_unchanged[x][3] for x in test_index])
                    assert sum(
                        [len(list(filter((lambda a: a[3] == fin_set[x][3] and a[0] not in train_index), structure))) for x in range(len(fin_set)) if index_window[0] <= x <= index_window[1]]) == jump
                    assert sum([labels[x] for x in train_index]) == len(train_index) // 2
                    assert sum([labels[x] for x in test_index]) == len(test_index) // 2
                    yield train_index, test_index

        if mode == 3:
            structure.sort(key=(lambda x: x[1]))
            tmp = [x for x in structure if 'd' not in x[2]]
            structure_dark = [x for x in structure if 'd' in x[2]]
            structure.clear()
            structure.extend(tmp)
            structure.sort(key=(lambda x: x[3]))
            structure_dark.sort(key=(lambda x: x[3]))

            length = len(structure) // 2
            rand_offset = [0 for x in range(length // 4)] + [1 for x in range(length // 4)]
            rand_offset2 = [0 for x in range(length // 4)] + [1 for x in range(length // 4)]
            rand_indices = [x for x in range(1, length + 1)]
            random.shuffle(rand_offset)
            random.shuffle(rand_offset2)
            random.shuffle(rand_indices)
            rand_offset = rand_offset + rand_offset2

            fin_set = []
            for i in range(0, len(rand_indices)):
                if i < 100:
                    samples = list(filter((lambda x: x[3] == rand_indices[i]), structure))
                    assert len(samples) == 2
                    fin_set.append(samples[rand_offset[i]])
                else:
                    samples = list(filter((lambda x: x[3] == rand_indices[i]), structure_dark))
                    assert len(samples) == 2
                    fin_set.append(samples[rand_offset[i]])

            random.shuffle(fin_set)
            fin_set.sort(key=(lambda x: x[1]))
            for i in range(0, len(fin_set) - 100, 2):
                fin_set[i], fin_set[i + 100] = fin_set[i + 100], fin_set[i]

            jump = len(fin_set) // n_splits
            for i in range(n_splits):
                index_window = ((i * jump), ((i + 1) * jump - 1))
                index_window_ignored = (((i - 1) * jump), (i * jump - 1)) if i != 0 else (((n_splits - 1) * jump), (n_splits * jump - 1))
                train_index = [fin_set[x][0] for x in range(len(fin_set)) if x < index_window_ignored[0] or x > index_window_ignored[1]]
                test_index = []
                for x in range(len(fin_set)):
                    if index_window[0] <= x <= index_window[1]:
                        if 'd' in fin_set[x][2]:
                            sample = list(filter((lambda a: a[3] == fin_set[x][3] and a[1] != fin_set[x][1]), structure))
                            assert len(sample) == 1
                        else:
                            sample = list(filter((lambda a: a[3] == fin_set[x][3] and a[1] != fin_set[x][1]), structure_dark))
                            assert len(sample) == 1
                        test_index.append(sample[0][0])
                assert len(set(train_index).union(set(test_index))) == len(fin_set)
                assert len(set(train_index).intersection(set(test_index))) == 0
                assert set([structure_unchanged[x][3] for x in train_index]).intersection(set([structure_unchanged[x][3] for x in test_index])) == \
                       set([structure_unchanged[x][3] for x in test_index])
                assert sum([labels[x] for x in train_index]) == len(train_index) // 2
                assert sum([labels[x] for x in test_index]) == len(test_index) // 2
                yield train_index, test_index

        if mode == 4:
            tmp = [x for x in structure if 'd' not in x[2]]
            structure.clear()
            structure.extend(tmp)
            random.shuffle(structure)
            structure.sort(key=(lambda x: x[1]))
            length = len(structure)
            for i in range(0, length // 2, 2):
                structure[i], structure[i + length // 2] = structure[i + length // 2], structure[i]
            fin_set = structure[:]
            jump = len(fin_set) // n_splits
            for i in range(n_splits):
                index_window = ((i * jump), ((i + 1) * jump - 1))
                train_index = [fin_set[x][0] for x in range(len(fin_set)) if x < index_window[0] or x > index_window[1]]
                test_index = [fin_set[x][0] for x in range(len(fin_set)) if index_window[0] <= x <= index_window[1]]
                assert len(set(train_index).union(set(test_index))) == len(fin_set)
                assert len(set(train_index).intersection(set(test_index))) == 0
                assert sum([labels[x] for x in train_index]) == len(train_index) // 2
                assert sum([labels[x] for x in test_index]) == len(test_index) // 2
                yield train_index, test_index

    def get_n_splits(self, X, y, groups=None):
        return self.n_splits


if __name__ == "__main__":

    features_folder = "caps_call_UDP"

    if len(sys.argv) <= 1:
        print("Folder name not provided, defaulting to", features_folder)
    else:
        features_folder = sys.argv[1]

    # example of folder structure: \extractedFeatures\caps_call_UDP\protozoa\PL_UDP.csv

    features_folder_path = 'extractedFeatures/' + features_folder + '/'
    feature_sets = os.listdir(features_folder_path)
    features = []

    for feature_set in feature_sets:
        feature_set_list = [feature_set]
        for feature_file in os.listdir(features_folder_path + feature_set):
            feature_set_list.append((feature_file, readCSV(features_folder_path + feature_set + '/' + feature_file), file_names[:]))
        features.append(feature_set_list)

    comparisons = list(itertools.combinations(features, 2))
    for comparison in comparisons:

        for model in models:
            print(model)
            for i in range(1, len(comparison[0])):
                print("Classifying " + comparison[0][0] + '-' + comparison[1][0] + " based on " + comparison[0][i][0])

                dataset = np.concatenate((comparison[0][i][1], comparison[1][i][1]))
                labels = np.array(([0] * len(comparison[0][1][1])) + ([1] * len(comparison[1][1][1])))
                names = comparison[0][i][2] + comparison[1][i][2]

                sel = VarianceThreshold(threshold=0)
                dataset = sel.fit_transform(dataset)


                parameters = {'eta': [0.3, 0.1, 0.01], 'max_depth': [6, 4], 'subsample': [1, 0.5],
                              'min_child_weight': [1, 3, 5], 'colsample_bytree': [1, 0.5, 0.2], 'lambda': [1, 1.2, 1.5]}
                generatorCV = CustomSplit(names, n_splits=5)
                gridSearch = GridSearchCV(model, parameters, scoring='roc_auc', n_jobs=-1, cv=generatorCV, verbose=1)
                gridSearch.fit(dataset, labels)
                model = gridSearch.best_estimator_

                tprs = []
                aucs = []
                mean_fpr = np.linspace(0, 1, 100)

                fold_count = 1
                for train_index, test_index in generatorCV.split(dataset, labels):
                    X_train = dataset[train_index]
                    X_test = dataset[test_index]
                    y_train = labels[train_index]
                    y_test = labels[test_index]

                    model.fit(X_train, y_train)
                    y_pred = model.predict(X_test)
                    accuracies = accuracy_score(y_test, y_pred)
                    y_prob = model.predict_proba(X_test)[:, 1]

                    fpr, tpr, thresholds = roc_curve(y_test, y_prob, pos_label=1)
                    tmp_auc = auc(fpr, tpr)

                    interp_tpr = np.interp(mean_fpr, fpr, tpr)
                    interp_tpr[0] = 0.0
                    tprs.append(interp_tpr)
                    aucs.append(tmp_auc)
                    print(fold_count, accuracies, tmp_auc)

                    y_pred_train = model.predict(X_train)
                    print("training acc:", accuracy_score(y_train, y_pred_train))


                mean_tpr = np.mean(tprs, axis=0)
                mean_tpr[-1] = 1.0
                mean_auc = auc(mean_fpr, mean_tpr)
                std_auc = np.std(aucs)

                fig, ax = plt.subplots()
                ax.plot([0, 1], [0, 1], transform=ax.transAxes, label="Random guess", linestyle="--", alpha=0.5)
                ax.plot(
                    mean_fpr,
                    mean_tpr,
                    color='r',
                    label="Mean ROC\n(AUC = %0.2f $\pm$ %0.2f)" % (mean_auc, std_auc),
                )

                std_tpr = np.std(tprs, axis=0)
                tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
                tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
                ax.fill_between(
                    mean_fpr,
                    tprs_lower,
                    tprs_upper,
                    color="grey",
                    alpha=0.2,
                    label=r"$\pm$ 1 std. dev.",
                )

                ax.set(
                    title="ROC using " + str(type(model)).split('.')[-1][:-2] + " on " +
                          comparison[0][i][0].split('_')[0] + " features"
                )
                ax.title.set_size(15)
                ax.set_xlabel("False positive rate")
                ax.set_ylabel("True positive rate")
                ax.legend(loc="upper left", prop={'size': 13})
                ax.grid()
                plt.show()

    print()
