import csv
import itertools
import os
import random
import sys

import matplotlib.pyplot as plt
import numpy as np

from sklearn.feature_selection import VarianceThreshold
from sklearn.metrics import accuracy_score, roc_curve, auc
from sklearn.model_selection import StratifiedKFold, GridSearchCV

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from xgboost import XGBClassifier

models = [
    XGBClassifier(),
    # RandomForestClassifier(),
    # DecisionTreeClassifier()
]

feature_names = []
file_names = []

chosen_files_call = ['sample_call_1.mp4_regular.pcap', 'sample_call_10.mp4_regular.pcap', 'sample_call_101_dark.mp4_regular.pcap', 'sample_call_102.mp4_regular.pcap', 'sample_call_103.mp4_regular.pcap', 'sample_call_103_dark.mp4_regular.pcap', 'sample_call_104_dark.mp4_regular.pcap', 'sample_call_105.mp4_regular.pcap', 'sample_call_105_dark.mp4_regular.pcap', 'sample_call_107_dark.mp4_regular.pcap', 'sample_call_109.mp4_regular.pcap', 'sample_call_10_dark.mp4_regular.pcap', 'sample_call_110.mp4_regular.pcap', 'sample_call_110_dark.mp4_regular.pcap', 'sample_call_112_dark.mp4_regular.pcap', 'sample_call_114.mp4_regular.pcap', 'sample_call_117.mp4_regular.pcap', 'sample_call_118.mp4_regular.pcap', 'sample_call_119.mp4_regular.pcap', 'sample_call_120.mp4_regular.pcap', 'sample_call_123_dark.mp4_regular.pcap', 'sample_call_124_dark.mp4_regular.pcap', 'sample_call_125_dark.mp4_regular.pcap', 'sample_call_126.mp4_regular.pcap', 'sample_call_127_dark.mp4_regular.pcap', 'sample_call_128.mp4_regular.pcap', 'sample_call_128_dark.mp4_regular.pcap', 'sample_call_129_dark.mp4_regular.pcap', 'sample_call_130.mp4_regular.pcap', 'sample_call_130_dark.mp4_regular.pcap', 'sample_call_132_dark.mp4_regular.pcap', 'sample_call_133.mp4_regular.pcap', 'sample_call_133_dark.mp4_regular.pcap', 'sample_call_134.mp4_regular.pcap', 'sample_call_134_dark.mp4_regular.pcap', 'sample_call_135_dark.mp4_regular.pcap', 'sample_call_136.mp4_regular.pcap', 'sample_call_137.mp4_regular.pcap', 'sample_call_137_dark.mp4_regular.pcap', 'sample_call_138.mp4_regular.pcap', 'sample_call_138_dark.mp4_regular.pcap', 'sample_call_139.mp4_regular.pcap', 'sample_call_139_dark.mp4_regular.pcap', 'sample_call_140.mp4_regular.pcap', 'sample_call_141.mp4_regular.pcap', 'sample_call_142.mp4_regular.pcap', 'sample_call_143_dark.mp4_regular.pcap', 'sample_call_144.mp4_regular.pcap', 'sample_call_145_dark.mp4_regular.pcap', 'sample_call_146.mp4_regular.pcap', 'sample_call_147.mp4_regular.pcap', 'sample_call_147_dark.mp4_regular.pcap', 'sample_call_148.mp4_regular.pcap', 'sample_call_150_dark.mp4_regular.pcap', 'sample_call_151.mp4_regular.pcap', 'sample_call_152_dark.mp4_regular.pcap', 'sample_call_154_dark.mp4_regular.pcap', 'sample_call_155.mp4_regular.pcap', 'sample_call_155_dark.mp4_regular.pcap', 'sample_call_156_dark.mp4_regular.pcap', 'sample_call_157.mp4_regular.pcap', 'sample_call_158.mp4_regular.pcap', 'sample_call_15_dark.mp4_regular.pcap', 'sample_call_16.mp4_regular.pcap', 'sample_call_160_dark.mp4_regular.pcap', 'sample_call_161.mp4_regular.pcap', 'sample_call_162.mp4_regular.pcap', 'sample_call_162_dark.mp4_regular.pcap', 'sample_call_163.mp4_regular.pcap', 'sample_call_166.mp4_regular.pcap', 'sample_call_166_dark.mp4_regular.pcap', 'sample_call_169_dark.mp4_regular.pcap', 'sample_call_16_dark.mp4_regular.pcap', 'sample_call_170.mp4_regular.pcap', 'sample_call_170_dark.mp4_regular.pcap', 'sample_call_171.mp4_regular.pcap', 'sample_call_172_dark.mp4_regular.pcap', 'sample_call_173.mp4_regular.pcap', 'sample_call_174.mp4_regular.pcap', 'sample_call_174_dark.mp4_regular.pcap', 'sample_call_175.mp4_regular.pcap', 'sample_call_175_dark.mp4_regular.pcap', 'sample_call_176_dark.mp4_regular.pcap', 'sample_call_178_dark.mp4_regular.pcap', 'sample_call_179.mp4_regular.pcap', 'sample_call_179_dark.mp4_regular.pcap', 'sample_call_17_dark.mp4_regular.pcap', 'sample_call_180.mp4_regular.pcap', 'sample_call_180_dark.mp4_regular.pcap', 'sample_call_181_dark.mp4_regular.pcap', 'sample_call_182.mp4_regular.pcap', 'sample_call_183.mp4_regular.pcap', 'sample_call_183_dark.mp4_regular.pcap', 'sample_call_185.mp4_regular.pcap', 'sample_call_188.mp4_regular.pcap', 'sample_call_188_dark.mp4_regular.pcap', 'sample_call_19.mp4_regular.pcap', 'sample_call_191.mp4_regular.pcap', 'sample_call_192_dark.mp4_regular.pcap', 'sample_call_193.mp4_regular.pcap', 'sample_call_193_dark.mp4_regular.pcap', 'sample_call_194.mp4_regular.pcap', 'sample_call_194_dark.mp4_regular.pcap', 'sample_call_195.mp4_regular.pcap', 'sample_call_197.mp4_regular.pcap', 'sample_call_198.mp4_regular.pcap', 'sample_call_198_dark.mp4_regular.pcap', 'sample_call_19_dark.mp4_regular.pcap', 'sample_call_1_dark.mp4_regular.pcap', 'sample_call_2.mp4_regular.pcap', 'sample_call_20.mp4_regular.pcap', 'sample_call_20_dark.mp4_regular.pcap', 'sample_call_21.mp4_regular.pcap', 'sample_call_21_dark.mp4_regular.pcap', 'sample_call_23_dark.mp4_regular.pcap', 'sample_call_25.mp4_regular.pcap', 'sample_call_25_dark.mp4_regular.pcap', 'sample_call_26.mp4_regular.pcap', 'sample_call_26_dark.mp4_regular.pcap', 'sample_call_27_dark.mp4_regular.pcap', 'sample_call_28.mp4_regular.pcap', 'sample_call_29.mp4_regular.pcap', 'sample_call_29_dark.mp4_regular.pcap', 'sample_call_30.mp4_regular.pcap', 'sample_call_30_dark.mp4_regular.pcap', 'sample_call_32.mp4_regular.pcap', 'sample_call_32_dark.mp4_regular.pcap', 'sample_call_34_dark.mp4_regular.pcap', 'sample_call_37_dark.mp4_regular.pcap', 'sample_call_38.mp4_regular.pcap', 'sample_call_38_dark.mp4_regular.pcap', 'sample_call_40.mp4_regular.pcap', 'sample_call_40_dark.mp4_regular.pcap', 'sample_call_41.mp4_regular.pcap', 'sample_call_42.mp4_regular.pcap', 'sample_call_43_dark.mp4_regular.pcap', 'sample_call_44_dark.mp4_regular.pcap', 'sample_call_46.mp4_regular.pcap', 'sample_call_47.mp4_regular.pcap', 'sample_call_48.mp4_regular.pcap', 'sample_call_49_dark.mp4_regular.pcap', 'sample_call_4_dark.mp4_regular.pcap', 'sample_call_51.mp4_regular.pcap', 'sample_call_51_dark.mp4_regular.pcap', 'sample_call_52.mp4_regular.pcap', 'sample_call_52_dark.mp4_regular.pcap', 'sample_call_53.mp4_regular.pcap', 'sample_call_54.mp4_regular.pcap', 'sample_call_54_dark.mp4_regular.pcap', 'sample_call_56.mp4_regular.pcap', 'sample_call_56_dark.mp4_regular.pcap', 'sample_call_57.mp4_regular.pcap', 'sample_call_58.mp4_regular.pcap', 'sample_call_58_dark.mp4_regular.pcap', 'sample_call_6.mp4_regular.pcap', 'sample_call_60_dark.mp4_regular.pcap', 'sample_call_61.mp4_regular.pcap', 'sample_call_62_dark.mp4_regular.pcap', 'sample_call_64.mp4_regular.pcap', 'sample_call_64_dark.mp4_regular.pcap', 'sample_call_65_dark.mp4_regular.pcap', 'sample_call_66_dark.mp4_regular.pcap', 'sample_call_67.mp4_regular.pcap', 'sample_call_67_dark.mp4_regular.pcap', 'sample_call_68.mp4_regular.pcap', 'sample_call_69.mp4_regular.pcap', 'sample_call_7.mp4_regular.pcap', 'sample_call_73.mp4_regular.pcap', 'sample_call_73_dark.mp4_regular.pcap', 'sample_call_74.mp4_regular.pcap', 'sample_call_75.mp4_regular.pcap', 'sample_call_75_dark.mp4_regular.pcap', 'sample_call_77.mp4_regular.pcap', 'sample_call_77_dark.mp4_regular.pcap', 'sample_call_79_dark.mp4_regular.pcap', 'sample_call_7_dark.mp4_regular.pcap', 'sample_call_8.mp4_regular.pcap', 'sample_call_80.mp4_regular.pcap', 'sample_call_80_dark.mp4_regular.pcap', 'sample_call_81.mp4_regular.pcap', 'sample_call_82.mp4_regular.pcap', 'sample_call_82_dark.mp4_regular.pcap', 'sample_call_85.mp4_regular.pcap', 'sample_call_85_dark.mp4_regular.pcap', 'sample_call_86.mp4_regular.pcap', 'sample_call_86_dark.mp4_regular.pcap', 'sample_call_89.mp4_regular.pcap', 'sample_call_8_dark.mp4_regular.pcap', 'sample_call_9.mp4_regular.pcap', 'sample_call_91.mp4_regular.pcap', 'sample_call_91_dark.mp4_regular.pcap', 'sample_call_92_dark.mp4_regular.pcap', 'sample_call_94.mp4_regular.pcap', 'sample_call_94_dark.mp4_regular.pcap', 'sample_call_95.mp4_regular.pcap', 'sample_call_95_dark.mp4_regular.pcap', 'sample_call_97.mp4_regular.pcap', 'sample_call_97_dark.mp4_regular.pcap', 'sample_call_98.mp4_regular.pcap', 'sample_call_99_dark.mp4_regular.pcap']
chosen_files_dash = ['sample_dash_10.mp4_regular.pcap', 'sample_dash_100.mp4_regular.pcap', 'sample_dash_101.mp4_regular.pcap', 'sample_dash_101_dark.mp4_regular.pcap', 'sample_dash_102_dark.mp4_regular.pcap', 'sample_dash_104.mp4_regular.pcap', 'sample_dash_104_dark.mp4_regular.pcap', 'sample_dash_106.mp4_regular.pcap', 'sample_dash_106_dark.mp4_regular.pcap', 'sample_dash_107.mp4_regular.pcap', 'sample_dash_108.mp4_regular.pcap', 'sample_dash_108_dark.mp4_regular.pcap', 'sample_dash_109_dark.mp4_regular.pcap', 'sample_dash_111.mp4_regular.pcap', 'sample_dash_112.mp4_regular.pcap', 'sample_dash_112_dark.mp4_regular.pcap', 'sample_dash_114.mp4_regular.pcap', 'sample_dash_115.mp4_regular.pcap', 'sample_dash_115_dark.mp4_regular.pcap', 'sample_dash_116_dark.mp4_regular.pcap', 'sample_dash_117.mp4_regular.pcap', 'sample_dash_117_dark.mp4_regular.pcap', 'sample_dash_11_dark.mp4_regular.pcap', 'sample_dash_12.mp4_regular.pcap', 'sample_dash_121.mp4_regular.pcap', 'sample_dash_121_dark.mp4_regular.pcap', 'sample_dash_123.mp4_regular.pcap', 'sample_dash_123_dark.mp4_regular.pcap', 'sample_dash_124_dark.mp4_regular.pcap', 'sample_dash_125_dark.mp4_regular.pcap', 'sample_dash_127_dark.mp4_regular.pcap', 'sample_dash_128_dark.mp4_regular.pcap', 'sample_dash_129.mp4_regular.pcap', 'sample_dash_129_dark.mp4_regular.pcap', 'sample_dash_12_dark.mp4_regular.pcap', 'sample_dash_13.mp4_regular.pcap', 'sample_dash_130_dark.mp4_regular.pcap', 'sample_dash_131_dark.mp4_regular.pcap', 'sample_dash_133.mp4_regular.pcap', 'sample_dash_134.mp4_regular.pcap', 'sample_dash_134_dark.mp4_regular.pcap', 'sample_dash_135.mp4_regular.pcap', 'sample_dash_137.mp4_regular.pcap', 'sample_dash_137_dark.mp4_regular.pcap', 'sample_dash_138.mp4_regular.pcap', 'sample_dash_138_dark.mp4_regular.pcap', 'sample_dash_139.mp4_regular.pcap', 'sample_dash_140.mp4_regular.pcap', 'sample_dash_141.mp4_regular.pcap', 'sample_dash_142.mp4_regular.pcap', 'sample_dash_142_dark.mp4_regular.pcap', 'sample_dash_143.mp4_regular.pcap', 'sample_dash_144.mp4_regular.pcap', 'sample_dash_144_dark.mp4_regular.pcap', 'sample_dash_145.mp4_regular.pcap', 'sample_dash_14_dark.mp4_regular.pcap', 'sample_dash_150.mp4_regular.pcap', 'sample_dash_151_dark.mp4_regular.pcap', 'sample_dash_156_dark.mp4_regular.pcap', 'sample_dash_16.mp4_regular.pcap', 'sample_dash_160_dark.mp4_regular.pcap', 'sample_dash_162.mp4_regular.pcap', 'sample_dash_165_dark.mp4_regular.pcap', 'sample_dash_167_dark.mp4_regular.pcap', 'sample_dash_16_dark.mp4_regular.pcap', 'sample_dash_17.mp4_regular.pcap', 'sample_dash_171_dark.mp4_regular.pcap', 'sample_dash_173.mp4_regular.pcap', 'sample_dash_173_dark.mp4_regular.pcap', 'sample_dash_174.mp4_regular.pcap', 'sample_dash_174_dark.mp4_regular.pcap', 'sample_dash_175.mp4_regular.pcap', 'sample_dash_175_dark.mp4_regular.pcap', 'sample_dash_176.mp4_regular.pcap', 'sample_dash_177_dark.mp4_regular.pcap', 'sample_dash_178.mp4_regular.pcap', 'sample_dash_179.mp4_regular.pcap', 'sample_dash_17_dark.mp4_regular.pcap', 'sample_dash_18.mp4_regular.pcap', 'sample_dash_180.mp4_regular.pcap', 'sample_dash_181.mp4_regular.pcap', 'sample_dash_181_dark.mp4_regular.pcap', 'sample_dash_182_dark.mp4_regular.pcap', 'sample_dash_183_dark.mp4_regular.pcap', 'sample_dash_184.mp4_regular.pcap', 'sample_dash_184_dark.mp4_regular.pcap', 'sample_dash_185.mp4_regular.pcap', 'sample_dash_185_dark.mp4_regular.pcap', 'sample_dash_186.mp4_regular.pcap', 'sample_dash_186_dark.mp4_regular.pcap', 'sample_dash_187_dark.mp4_regular.pcap', 'sample_dash_188_dark.mp4_regular.pcap', 'sample_dash_189.mp4_regular.pcap', 'sample_dash_189_dark.mp4_regular.pcap', 'sample_dash_18_dark.mp4_regular.pcap', 'sample_dash_19.mp4_regular.pcap', 'sample_dash_190.mp4_regular.pcap', 'sample_dash_190_dark.mp4_regular.pcap', 'sample_dash_191.mp4_regular.pcap', 'sample_dash_191_dark.mp4_regular.pcap', 'sample_dash_192.mp4_regular.pcap', 'sample_dash_193.mp4_regular.pcap', 'sample_dash_194.mp4_regular.pcap', 'sample_dash_195.mp4_regular.pcap', 'sample_dash_195_dark.mp4_regular.pcap', 'sample_dash_196.mp4_regular.pcap', 'sample_dash_197.mp4_regular.pcap', 'sample_dash_199.mp4_regular.pcap', 'sample_dash_199_dark.mp4_regular.pcap', 'sample_dash_19_dark.mp4_regular.pcap', 'sample_dash_2.mp4_regular.pcap', 'sample_dash_200_dark.mp4_regular.pcap', 'sample_dash_201_dark.mp4_regular.pcap', 'sample_dash_202.mp4_regular.pcap', 'sample_dash_202_dark.mp4_regular.pcap', 'sample_dash_23_dark.mp4_regular.pcap', 'sample_dash_24.mp4_regular.pcap', 'sample_dash_27_dark.mp4_regular.pcap', 'sample_dash_29_dark.mp4_regular.pcap', 'sample_dash_3.mp4_regular.pcap', 'sample_dash_30.mp4_regular.pcap', 'sample_dash_30_dark.mp4_regular.pcap', 'sample_dash_31.mp4_regular.pcap', 'sample_dash_31_dark.mp4_regular.pcap', 'sample_dash_32_dark.mp4_regular.pcap', 'sample_dash_33.mp4_regular.pcap', 'sample_dash_34.mp4_regular.pcap', 'sample_dash_35.mp4_regular.pcap', 'sample_dash_36.mp4_regular.pcap', 'sample_dash_36_dark.mp4_regular.pcap', 'sample_dash_37_dark.mp4_regular.pcap', 'sample_dash_38_dark.mp4_regular.pcap', 'sample_dash_39_dark.mp4_regular.pcap', 'sample_dash_3_dark.mp4_regular.pcap', 'sample_dash_40_dark.mp4_regular.pcap', 'sample_dash_42.mp4_regular.pcap', 'sample_dash_42_dark.mp4_regular.pcap', 'sample_dash_43.mp4_regular.pcap', 'sample_dash_44.mp4_regular.pcap', 'sample_dash_44_dark.mp4_regular.pcap', 'sample_dash_45.mp4_regular.pcap', 'sample_dash_45_dark.mp4_regular.pcap', 'sample_dash_46_dark.mp4_regular.pcap', 'sample_dash_47_dark.mp4_regular.pcap', 'sample_dash_48_dark.mp4_regular.pcap', 'sample_dash_49_dark.mp4_regular.pcap', 'sample_dash_4_dark.mp4_regular.pcap', 'sample_dash_50.mp4_regular.pcap', 'sample_dash_50_dark.mp4_regular.pcap', 'sample_dash_52.mp4_regular.pcap', 'sample_dash_52_dark.mp4_regular.pcap', 'sample_dash_53.mp4_regular.pcap', 'sample_dash_53_dark.mp4_regular.pcap', 'sample_dash_54_dark.mp4_regular.pcap', 'sample_dash_55_dark.mp4_regular.pcap', 'sample_dash_56_dark.mp4_regular.pcap', 'sample_dash_57.mp4_regular.pcap', 'sample_dash_57_dark.mp4_regular.pcap', 'sample_dash_5_dark.mp4_regular.pcap', 'sample_dash_6.mp4_regular.pcap', 'sample_dash_60_dark.mp4_regular.pcap', 'sample_dash_61.mp4_regular.pcap', 'sample_dash_61_dark.mp4_regular.pcap', 'sample_dash_62.mp4_regular.pcap', 'sample_dash_62_dark.mp4_regular.pcap', 'sample_dash_63.mp4_regular.pcap', 'sample_dash_66.mp4_regular.pcap', 'sample_dash_71_dark.mp4_regular.pcap', 'sample_dash_72.mp4_regular.pcap', 'sample_dash_72_dark.mp4_regular.pcap', 'sample_dash_73.mp4_regular.pcap', 'sample_dash_75.mp4_regular.pcap', 'sample_dash_75_dark.mp4_regular.pcap', 'sample_dash_7_dark.mp4_regular.pcap', 'sample_dash_8.mp4_regular.pcap', 'sample_dash_81.mp4_regular.pcap', 'sample_dash_83.mp4_regular.pcap', 'sample_dash_83_dark.mp4_regular.pcap', 'sample_dash_84.mp4_regular.pcap', 'sample_dash_84_dark.mp4_regular.pcap', 'sample_dash_85.mp4_regular.pcap', 'sample_dash_85_dark.mp4_regular.pcap', 'sample_dash_86.mp4_regular.pcap', 'sample_dash_86_dark.mp4_regular.pcap', 'sample_dash_87.mp4_regular.pcap', 'sample_dash_88.mp4_regular.pcap', 'sample_dash_88_dark.mp4_regular.pcap', 'sample_dash_89.mp4_regular.pcap', 'sample_dash_89_dark.mp4_regular.pcap', 'sample_dash_8_dark.mp4_regular.pcap', 'sample_dash_90_dark.mp4_regular.pcap', 'sample_dash_91_dark.mp4_regular.pcap', 'sample_dash_92_dark.mp4_regular.pcap', 'sample_dash_94.mp4_regular.pcap', 'sample_dash_95.mp4_regular.pcap', 'sample_dash_97.mp4_regular.pcap', 'sample_dash_98.mp4_regular.pcap', 'sample_dash_98_dark.mp4_regular.pcap', 'sample_dash_99_dark.mp4_regular.pcap', 'sample_dash_9_dark.mp4_regular.pcap']

def readCSV(file_path):
    file_names.clear()
    file = open(file_path, 'r')
    lst = list(csv.reader(file, delimiter=','))
    data2 = lst[1:]
    data = []
    if 'regular_call' in file_path:
        for sample in data2:
            if sample[0] in chosen_files_call:
                data.append(sample)
    if 'regular_dash' in file_path:
        for sample in data2:
            if sample[0] in chosen_files_dash:
                data.append(sample)
    feature_names.append(lst[0])
    data_np = np.array(data)[:, 1:-1].astype(float)
    # label = data[0][-1]
    for row in data:
        file_names.append(row[0])
    file.close()
    for i, file in enumerate(file_names):
        num = file.split('_')[2] if 'dark' in file else file.split('_')[2][:-4]
        type = 'r' if 'regular' in file else 'p'
        col = 'd' if 'dark' in file else 'b'

        file_names[i] = num
    return data_np


class CustomSplit:
    def __init__(self, files, n_splits=5):
        self.files = files
        self.n_splits = n_splits

    def split(self, dataset, labels, groups=None):

        skf = StratifiedKFold(n_splits=n_splits, shuffle=True)
        for train_index, test_index in skf.split(dataset, labels):
            yield train_index, test_index

    def get_n_splits(self, X, y, groups=None):
        return self.n_splits


if __name__ == "__main__":

    features_folder = "dash_call_comp"

    if len(sys.argv) <= 1:
        print("Folder name not provided, defaulting to", features_folder)
    else:
        features_folder = sys.argv[1]

    features_folder_path = 'extractedFeatures/' + features_folder + '/'
    feature_sets = os.listdir(features_folder_path)
    features = []

    for feature_set in feature_sets:
        feature_set_list = [feature_set]
        for feature_file in os.listdir(features_folder_path + feature_set):
            feature_set_list.append((feature_file, readCSV(features_folder_path + feature_set + '/' + feature_file), file_names[:]))
        features.append(feature_set_list)

    comparisons = list(itertools.combinations(features, 2))
    for comparison in comparisons:

        for model in models:
            print(model)
            for i in range(1, len(comparison[0])):
                print("Classifying " + comparison[0][0] + '-' + comparison[1][0] + " based on " + comparison[0][i][0])

                dataset = np.concatenate((comparison[0][i][1], comparison[1][i][1]))
                labels = np.array(([0] * len(comparison[0][1][1])) + ([1] * len(comparison[1][1][1])))
                names = comparison[0][i][2] + comparison[1][i][2]

                sel = VarianceThreshold(threshold=0)
                dataset = sel.fit_transform(dataset)

                tprs = []
                aucs = []
                mean_fpr = np.linspace(0, 1, 100)

                generatorCV = StratifiedKFold(n_splits=5, shuffle=True)

                fold_count = 1
                for train_index, test_index in generatorCV.split(dataset, labels):
                    # for train_index, test_index in skf.split(dataset, labels):
                    X_train = dataset[train_index]
                    X_test = dataset[test_index]
                    y_train = labels[train_index]
                    y_test = labels[test_index]

                    model.fit(X_train, y_train)
                    y_pred = model.predict(X_test)
                    accuracies = accuracy_score(y_test, y_pred)
                    y_prob = model.predict_proba(X_test)[:, 1]

                    fpr, tpr, thresholds = roc_curve(y_test, y_prob, pos_label=1)
                    tmp_auc = auc(fpr, tpr)

                    interp_tpr = np.interp(mean_fpr, fpr, tpr)
                    interp_tpr[0] = 0.0
                    tprs.append(interp_tpr)
                    aucs.append(tmp_auc)
                    print(fold_count, accuracies, tmp_auc)

                    y_pred_train = model.predict(X_train)
                    print("training acc:", accuracy_score(y_train, y_pred_train))

                    # exit(0)

                mean_tpr = np.mean(tprs, axis=0)
                mean_tpr[-1] = 1.0
                mean_auc = auc(mean_fpr, mean_tpr)
                std_auc = np.std(aucs)

                fig, ax = plt.subplots()
                ax.plot([0, 1], [0, 1], transform=ax.transAxes, label="Random guess", linestyle="--", alpha=0.5)
                ax.plot(
                    mean_fpr,
                    mean_tpr,
                    color='r',
                    label="Mean ROC\n(AUC = %0.2f $\pm$ %0.2f)" % (mean_auc, std_auc),
                )

                std_tpr = np.std(tprs, axis=0)
                tprs_upper = np.minimum(mean_tpr + std_tpr, 1)
                tprs_lower = np.maximum(mean_tpr - std_tpr, 0)
                ax.fill_between(
                    mean_fpr,
                    tprs_lower,
                    tprs_upper,
                    color="grey",
                    alpha=0.2,
                    label=r"$\pm$ 1 std. dev.",
                )

                ax.set(
                    title="ROC using " + str(type(model)).split('.')[-1][:-2] + " on " +
                          comparison[0][i][0].split('_')[0] + " features"
                )
                ax.title.set_size(15)
                ax.set_xlabel("False positive rate")
                ax.set_ylabel("True positive rate")
                ax.legend(prop={'size': 13})
                ax.grid()
                plt.show()

    print()
