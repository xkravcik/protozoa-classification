import datetime
import sys
import subprocess
import time
import pyautogui
import os
import random
from fabric import Connection
import paramiko

linux_home_folder = r"/home/editor/"
shared_folder = r"/media/sf_shared-ubuntu-server/"
video_folder = r"C:\home\shared-ubuntu-server\videos"
client_ip = "192.168.10.10"
server_ip = "192.168.20.20"
username = "editor"
password = ""


def execute_ssh(video, mode, room_code):

    client_command = "echo " + password + " | sudo -S python3.8 " + shared_folder + "client.py " + video + " " + mode + " " + room_code
    server_command = "echo " + password + " | sudo -S python3.8 " + shared_folder + "server.py " + video + " " + mode + " " + room_code

    print(client_command)
    print(server_command)

    client_connection = Connection(username + "@" + client_ip)
    server_connection = Connection(username + "@" + server_ip)


    try:
        print(str(datetime.datetime.now()) + " running client+server processes " + mode)
        client_process = client_connection.run(client_command, asynchronous=True)
        server_process = server_connection.run(server_command, asynchronous=True)

        # client_process.join()

        server_process.join()
    except Exception as e:
        print("exception when running commands:", e)
    finally:
        print(str(datetime.datetime.now()) + " closing connection")

        client_connection.close()
        server_connection.close()

    return

if __name__ == "__main__":

    #time.sleep(60 * 60 * 6)

    videos = os.listdir(video_folder)
    videos.sort()
    #random.shuffle(videos)
    random.seed()
    room_code_base = random.randint(100,999)

    counter = 0

    for video in videos:
        if counter >= 105:
            print("counter limit reached")
            exit(10)
        print(str(datetime.datetime.now()) + " starting " + str(video))
        room_code = str(room_code_base) + video # str(random.randint(1000000000,9999999999))
        execute_ssh(video, "regular", room_code + "regular")
        time.sleep(5)
        execute_ssh(video, "protozoa", room_code + "protozoa")
        print(str(datetime.datetime.now()) + " ending " + str(video))
        counter += 1
