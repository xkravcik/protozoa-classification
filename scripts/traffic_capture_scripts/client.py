import sys
import subprocess
import time
import os
import datetime
os.environ['DISPLAY'] = ':0'
from keyboard import press
import pyautogui
import os
from pynput.keyboard import Key, Controller

log_protozoa = open('log_protozoa', 'w')
log_ffmpeg = open('log_ffmpeg', 'w')
log_chromium = open('log_chromium', 'w')
log_capture = open('log_capture', 'w')
log_side = open('/media/sf_shared-ubuntu-server/logs/log_side_client', 'w', buffering=1)
log_all = open('/media/sf_shared-ubuntu-server/logs/log_all_client', 'w', buffering=1)

keyboard = Controller()

video_folder = "/media/sf_shared-ubuntu-server/videos/"
protozoa_folder = "/home/editor/Protozoa/protozoa/bin/"
chromium_builds_folder = "/home/editor/chromium_builds/"
webrtc_website = "https://sylaps.com/room/"
client_ip = "192.168.100.10"
capture_duration = 35

headless_env = dict(os.environ)
headless_env['DISPLAY'] = ':0'


def automate(video, mode, room_code):
    log_all.write(str(datetime.datetime.now()) + " starting " + str(mode) + '\n')

    ffmpeg_command = "ffmpeg -nostats -re -i " + video_folder + video + r" -r 30 -vcodec rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0"
    log_all.write(str(datetime.datetime.now()) + " executing " + ffmpeg_command + '\n')
    ffmpeg_process = subprocess.Popen(ffmpeg_command, shell=True, stdout=log_ffmpeg, stderr=log_side, stdin = open(os.devnull))

    assert ffmpeg_process.poll() is None

    protozoa_process = None
    if mode == "protozoa":
        protozoa_command = protozoa_folder + "protozoa -m client"
        log_all.write(str(datetime.datetime.now()) + " executing: " + protozoa_command + '\n')
        protozoa_process = subprocess.Popen(protozoa_command, cwd=r"/home/editor/Protozoa/protozoa/bin", shell=True, stdout=log_side, stderr=log_side)
        assert protozoa_process.poll() is None

    chromium_command = chromium_builds_folder
    if mode == "protozoa":
        chromium_command += "protozoaReplacementFullFrame_build/chrome --disable-session-crashed-bubbles --disable-infobars --no-sandbox " + webrtc_website + room_code
    else:
        chromium_command += "regular_build/chrome --disable-session-crashed-bubbles --disable-infobars --no-sandbox " + webrtc_website + room_code
    log_all.write(str(datetime.datetime.now()) + " executing: " + chromium_command + '\n')
    chromium_process = subprocess.Popen(chromium_command, shell=True, stdout=log_side, stderr=log_side)

    if ffmpeg_process.poll() is not None or chromium_process.poll() is not None:
        if ffmpeg_process.poll() is not None:
            log_all.write(str(datetime.datetime.now()) + " ffmpeg_process.poll() is not None:" + '\n')
            log_all.write(str(ffmpeg_process.returncode) + ';' + str(ffmpeg_process.stdin) + ';' + str(ffmpeg_process.stdout) + ';' + str(ffmpeg_process.stderr))
        if chromium_process.poll() is not None:
            log_all.write(str(datetime.datetime.now()) + " chromium_process.poll() is not None:" + '\n')
            log_all.write(str(chromium_process.returncode) + ';' + str(chromium_process.stdin) + ';' + str(chromium_process.stdout) + ';' + str(chromium_process.stderr))

    #enter callroom
    time.sleep(8)
    # #pyautogui.write('client', interval=0.25)
    # #pyautogui.press('enter')
    # keyboard.press(Key.enter)
    # keyboard.release(Key.enter)
    pyautogui.click(x=1250, y=640)
    time.sleep(3)
    #pyautogui.press('enter')
    #keyboard.press(Key.enter)
    #keyboard.release(Key.enter)
    time.sleep(2)
    pyautogui.click(x=1711, y=662)
    time.sleep(1)
    #pyautogui.press('enter')
    keyboard.press(Key.enter)
    keyboard.release(Key.enter)
    time.sleep(8)

    capture_command = 'tcpdump ip host ' + client_ip + ' -i enp0s8 -G ' + str(capture_duration) + ' -W 1 -w ' + video + '_' + mode + '.pcap'

    log_all.write(str(datetime.datetime.now()) + " executing: " + capture_command + '\n')
    capture_process = subprocess.Popen(capture_command, cwd="/media/sf_shared-ubuntu-server/caps/", shell=True, stdout=log_side, stderr=log_side)


    time.sleep(capture_duration + 5)
    capture_process.poll()

    if capture_process.returncode != 0:
        log_capture.write("Capture Unsuccessful")
        log_all.write(str(datetime.datetime.now()) + " capture_process.returncode != 0 " + str(capture_process.returncode))

    capture_process.kill()
    os.system("pkill -9 -f chrome")
    chromium_process.kill()
    os.system("pkill -9 -f ffmpeg")
    ffmpeg_process.kill()
    if mode == "protozoa":
        protozoa_process.kill()

    time.sleep(2)

    log_all.write(str(datetime.datetime.now()) + " ending " + '\n')

    return

if __name__ == "__main__":
    video = sys.argv[1]
    mode = sys.argv[2]
    room_code = sys.argv[3]
    automate(video, mode, room_code)